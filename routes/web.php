<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/backend', 'BackendController@index')->name('backend.root');
Route::get('/home', 'BackendController@index')->name('home');
Route::get('/getusers', 'BackendController@getusers')->name('getusers');
Route::get('/deluser/{id}', 'BackendController@deluser')->name('deluser');


Auth::routes(['verify'=>true]);

Route::resource('/batch', 'Backend\BatchController');
Route::get('/loadbatch', 'Backend\BatchController@loadbatch')->name('loadbatch');

Route::resource('/courses', 'Backend\CourseController');
Route::get('/getsemcourse/{id}', 'Backend\CourseController@getsemcourse')->name('getsemcourse');
Route::get('/getmycourse/{id}', 'Backend\CourseController@getmycourse')->name('getmycourse');



Route::resource('/students', 'Backend\StudentController');
Route::get('/studentdetails/{id}', 'Backend\StudentController@details')->name('students.details');
Route::get('/showsectionstd/{id}', 'Backend\StudentController@showsectionstd')->name('students.showsectionstd');
Route::get('/setmarks/{session}/{section}/{mycourse}/{exam}', 'Backend\StudentController@setmarks')->name('setmarks');


Route::resource('/guard', 'Backend\GuardController');
Route::post('/saveguard/{id}', 'Backend\GuardController@saveguard')->name('saveguard');



Route::resource('/section', 'Backend\SectionController');
Route::post('/section/{year}', 'Backend\SectionController@savesection')->name('savesection');
Route::get('/getsection/{session}', 'Backend\SectionController@getsection')->name('getsection');
Route::get('/loadsectioninfo/{session}', 'Backend\SectionController@loadsectioninfo')->name('loadsectioninfo');

Route::get('/display/{section}', 'Backend\SectionController@display')->name('getmysection');

Route::resource('/semesters', 'Backend\SemesterController');

Route::resource('/sessions', 'Backend\SessionController');

Route::resource('/marks', 'Backend\ResultController');

Route::get('/getallmarks/{stid}/{sem}','Backend\ResultController@getallmarks')->name('getallmarks');

Route::resource('/teachers', 'Backend\TeacherController');

Route::resource('/staffs', 'Backend\StaffController');
Route::get('/staffpost', 'Backend\StaffController@staffpost');


Route::resource('/exams', 'Backend\ExamController');

Route::resource('/examsroutine', 'Backend\ExamRotuineController');
Route::get('/examsubjects/{session}/{exam}', 'Backend\ExamRotuineController@examsubjects');



Route::get('{path}', 'BackendController@index')->where('path' , '([A-z\d-\/_.]+)?')->name('backend.root');

// Route::get('/home', 'HomeController@index')->name('home');
