<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guard extends Model
{
    protected $fillable = [
        'relation','address','student_id'
    ]; 

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    public function student()
    {
        return $this->hasmany('App\Student');
    }


}
