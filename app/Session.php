<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'batch_id','semester_id'
    ]; 

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function semester()
    {
        return $this->belongsTo('App\Semester');
    }
    
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}
