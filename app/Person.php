<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{ 
    protected $fillable = [
        'fname', 'mname', 'lname','contact', 'email', 'citizenship','img', 
    ];

    

    public function student()
    {
        return $this->hasMany('App\Student');
    }


    public function guardgain()
    {
        return $this->hasmany('App\Guard');
    }


    public function staff()
    {
        return $this->hasmany('App\Staff');
    }
}