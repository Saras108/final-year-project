<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Course;
use App\Session;
use App\ExamRotuine;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Course::with('semester')->get();        
    }

    public function getsemcourse($id)
    {
        $getsession = Session::findOrFail($id);
        $getsem = $getsession->semester_id;
        return Course::where('semester_id', $getsem)->get();
    }

    public function getmycourse($getsem)
    {
        return Course::where('semester_id', $getsem)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {

        $semester = explode('_',$request->semester_id);
        $semester_id = $semester[0];
        
        $course = new Course();

        $course->subject_code = $request->subject_code;
        $course->subject = $request->subject;
        $course->semester_id = $semester_id;

        $course->save();   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = ExamRotuine::findorFail($id)->course_id;
        $course = Course::findOrFail($exam);
        return $course;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        
        if($course->semester_id!=$request->semester_id)
        {   
            $semester = explode('_',$request->semester_id);
            $semester_id = $semester[0];

            $course->semester_id =$semester_id;
        }else{
            $course->semester_id =$course->semester_id;
            
        }
        
        $course->subject_code = $request->subject_code;
        $course->subject = $request->subject;

        $course->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();
    }
}
