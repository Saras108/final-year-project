<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Result;
use App\Student;
use App\Session;
use App\Course;
use App\Exam;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $counter = count($request->markssetter);
        // dd($counter);
// dd($request->all());
        for($i=0; $i<$counter; $i++){
            
            if($request->myid[$i]){
                $getid = $request->myid[$i];
                $result = Result::where('id',$getid)->first();
            }else{
                $result = new Result();
                $result->course_id = $request->course;
                $result->exam_id = $request->exam;
                $result->session_id = $request->session;
                $result->student_id = $request->student[$i];
            }
            if(isset($request->markssetter[$i])){
                $result->marks = $request->markssetter[$i];
            }else{
                $result->marks = 'Abs';
            }
            // dd($result);
            $result->save();
        }

        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id;
    }

    public function getallmarks($stdid, $sem)
    {        
        // $getallcourse =  Course::where('semester_id', $sem)->get();
        // $getallexam =  Exam::all();

        $batch = Student::findOrFail($stdid)->batch_id;
        $sessionslug = $batch.$sem;

        $getsession = Session::where('slug',$sessionslug)->first()->id;

        $myresult = Result::where(['student_id'=>$stdid , 'session_id'=>$getsession])->with('exam')->get();

        return $myresult;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
