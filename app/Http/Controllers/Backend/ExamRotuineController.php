<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExamRotuine;
use DB;
class ExamRotuineController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function examsubjects($session , $exam)
    {
        $examlist = DB::table('exam_rotuines')
            ->where(['session_id'=>$session , 'exam_id'=>$exam])
            ->join('courses', 'exam_rotuines.course_id', '=', 'courses.id')
            ->select('exam_rotuines.*', 'courses.subject')
            ->get();

        return $examlist;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $getcourse = explode('_', $request->course_id);
        
        $data = new ExamRotuine();
        $data->exam_id = $request->exam_id;
        $data->course_id = $getcourse[0];
        $data->session_id = $request->session_id;
        $data->date = $request->date;
        $data->end_time = $request->end_time;
        $data->start_time = $request->start_time;
        
        $data->save(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ExamRotuine::findOrFail($id);

        if($data->course_id == $request->course_id){
            $getcourse = $data->course_id;
        }else{
            $getmycourse = explode('_', $request->course_id);
            $getcourse= $getmycourse[0];
        }
        
        $data->exam_id = $request->exam_id;
        $data->course_id = $getcourse;
        $data->session_id = $request->session_id;
        $data->date = $request->date;
        $data->end_time = $request->end_time;
        $data->start_time = $request->start_time;
        
        $data->save(); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
