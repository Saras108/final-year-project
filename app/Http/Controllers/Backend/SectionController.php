<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SectionRequest;
use App\Section;
use App\Session;
use App\Batch;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Section::whereBatchId($id)->get();
    }

    
    public function display($id)
    {
        return Section::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = Section::findOrFail($id);
        $section->section_name = $request->section_name;

        $section->save();

        return $section;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::findOrFail($id);
        $section->delete();
                
    }

    public function savesection(SectionRequest $request, $year)
    {
        $countbatch = Batch::findOrFail($year)->section_no;

        $countsection = Section::where('batch_id',$year)->count();
        // return $countsection;

        if($countbatch > $countsection){
            Section::create([
                'batch_id' => $year,
                'section_name' => $request->section_name,
            ]);
            $msg = "Section Added.";
            
        }else{
            $msg = "Section Already Filled" ;     
        }
        return $msg;
        
    }

    public function getsection($session)
    {
        $batch = Session::findOrFail($session)->batch_id;
        // dd($batch);
        $section = Section::where('batch_id',$batch)->get();
     
        return $section;
    }

    public function loadsectioninfo($id)
    {

        $session = Session::findOrFail($id)->batch_id;
        $batch = Batch::findOrFail($session);
        return $batch;

        
    }
}
