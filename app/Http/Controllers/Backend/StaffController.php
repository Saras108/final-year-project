<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Person;
use App\Staff;

class StaffController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Staff::with('person')->get();
    }

    public function staffpost()
    {
        $post = degination();
        return $post;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $person = new Person();
        $person->fname = $request->fname;
        $person->mname = $request->mname;
        $person->lname = $request->lname;
        $person->contact = $request->contact;
        $person->email = $request->email;
        $person->citizenship = $request->citizenship;
        
        $staff = new Staff();

        $staff->degination =$request->degination;
        
        $person->save();

        $person_id = $person->id;

        $staff->person_id =$person_id;

        $staff->save();
        return $staff->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $staff = Staff::findOrFail($id);
        $staff->degination =$request->degination;

        $person_id = $staff->person_id;

        $person = Person::findOrFail($person_id);
        
        $person->fname = $request->fname;
        $person->mname = $request->mname;
        $person->lname = $request->lname;
        $person->contact = $request->contact;
        $person->email = $request->email;
        $person->citizenship = $request->citizenship;

        $person->save();

        $staff->save();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);
        $staff->delete();

    }
}
