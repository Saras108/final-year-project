<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Student;
use App\Person;
use App\Session;
use App\Section;
use App\ExamRotuine;
use App\Course;
use App\Result;


class StudentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function setmarks($session , $section, $course , $exam )
    {
        // dd($course,$session,$exam);
        
        $students = Student::where('section_id',$section)->with(['batch', 'section' , 'person'])->orderBy('roll_no', 'asc')->get();

        $batch = Session::findOrFail($session)->batch_id;
        $section = Section::findOrFail($section);

        $info = Session::with('semester','batch')->findOrFail($session);
        
            foreach ($students as $student) {
                $marks[] = Result::where(['session_id'=>$session, 'exam_id'=>$exam, 'student_id'=>$student->id, 'course_id'=>$course])->get();
        
            };
            // dd($marks[0]);
            // dd($marks[0]->isEmpty());
            // dd($session,$exam,$course);
            // dd($marks[0][0]);

        $exam = ExamRotuine::findorFail($exam);
            
        $course = Course::findOrFail($exam->course_id);

        if(!$marks[0]->isEmpty()){
            return view('backend.pages.result.editresult', compact('section','info','course','students','session','marks','exam'));
        }else{
            return view('backend.pages.result.setresult', compact('section','info','course','students','session','marks','exam'));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::with(['batch', 'section' , 'person'])->get();
        return $students;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $fileName='';
        if($request->image){
            $imgexplode = explode(',',$request->image);
            $decodeimg = base64_decode($imgexplode[1]);

            if(str_contains($decodeimg[0],'jpeg'))
                $extension = 'jpg';
            else
                $extension = 'png';

            $fileName = str_random().'.'.$extension;

            $path = public_path().'/'.$fileName;
            file_put_contents($path , $decodeimg);

        }

      $getdata = explode('_', $request->section_id);


      $batch_id = $getdata[2];
      $section_id = $getdata[0];

      $getroll = Student::where(['batch_id'=>$batch_id, 'section_id'=>$section_id])->orderBy('roll_no', 'desc')->first();
      if($getroll!=''){
        $roll = $getroll->roll_no+1;
      }else{
        $roll = 1;
      }

         $person = new Person();
         $person->fname = $request->fname;
         $person->mname = $request->mname;
         $person->lname = $request->lname;
         $person->contact = $request->contact;
         $person->email = $request->email;
         $person->img = $fileName;
         $person->citizenship = $request->citizenship;

        //  dd($person);

         
         $student = new Student();

         $student->slc_marks =$request->slc_marks;
         $student->intermediate_marks =$request->intermediate_marks;
         $student->slc_batch =$request->slc_batch;
         $student->intermediate_batch =$request->intermediate_batch;
         $student->slc_symbol =$request->slc_symbol;
         $student->intermediate_symbol =$request->intermediate_symbol;
         $student->p_address =$request->p_address;
         $student->t_address =$request->t_address;
         $student->section_id =$section_id;
         $student->batch_id =$batch_id;
         $student->roll_no =$roll;

// dd($person->img);
         $person->save();

         $person_id = $person->id;
         $student->person_id =$person_id;

         $student->save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where('batch_id',$id)->with(['batch', 'section' , 'person'])->get();
        return $student;
    }

    public function showsectionstd($id)
    {
        $student = Student::where('section_id',$id)->with(['batch', 'section' , 'person'])->get();
        return $student;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, $id)
    {
        $student = Student::findOrFail($id);

        if($student->section_id!=$request->section_id)
        {   
            $getdata = explode('_', $request->section_id);

            $batch_id = $getdata[2];
            $section_id = $getdata[0];
                
            $student->batch_id =$batch_id;
            $student->section_id =$section_id;
        }else{
            $student->section_id =$request->section_id;
        }

         $person = Person::findOrFail($student->person_id);
         $person->fname = $request->fname;
         $person->mname = $request->mname;
         $person->lname = $request->lname;
         $person->contact = $request->contact;
         $person->email = $request->email;
         $person->citizenship = $request->citizenship;

         if($request->image){
            $imgexplode = explode(',',$request->image);
            $decodeimg = base64_decode($imgexplode[1]);

            if(str_contains($decodeimg[0],'jpeg'))
                $extension = 'jpg';
            else
                $extension = 'png';

            $fileName = str_random().'.'.$extension;

            $path = public_path().'/'.$fileName;
            file_put_contents($path , $decodeimg);



            // $image_path = public_path().'/'.$person->img;
            // if(File::exists($image_path)) {
            //     File::delete($image_path);
            // }
          
            

            $person->img = $fileName;

        }

         $student->slc_marks =$request->slc_marks;
         $student->intermediate_marks =$request->intermediate_marks;
         $student->slc_batch =$request->slc_batch;
         $student->intermediate_batch =$request->intermediate_batch;
         $student->slc_symbol =$request->slc_symbol;
         $student->intermediate_symbol =$request->intermediate_symbol;
         $student->p_address =$request->p_address;
         $student->t_address =$request->t_address;


         $person->save();


         $person_id = $person->id;
         $student->person_id =$person_id;

         $student->save();

         return $student;
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function details($id)
    {
        return Student::with(['batch', 'section' , 'person', 'guardgain'])->findOrFail($id);
    }
}
