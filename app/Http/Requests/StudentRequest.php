<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'citizenship'=>'required',
            'contact'=>'required',
            'email'=>'required',
            'fname'=>'required',
            'lname'=>'required',

            'section_id'=>'required',
            
            'slc_batch'=>'required',
            'slc_marks'=>'required',
            'slc_symbol'=>'required',
            'intermediate_batch'=>'required',
            'intermediate_marks'=>'required',
            'intermediate_symbol'=>'required',
            
            'p_address'=>'required',
            't_address'=>'required',
        ];
    }
}

