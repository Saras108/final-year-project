<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'batch_id','section_name'
    ];

    public function student()
    {
        return $this->hasMany('App\Student');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

}
