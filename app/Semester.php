<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $fillable = [
        'semester' 
    ];
    
    public function course()
    {
        return $this->hasMany('App\Course');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch','batch_id');
    }

}
