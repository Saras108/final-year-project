<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = [
        'enrolled_year','student_no','section_no'
    ];

    public function student()
    {
        return $this->hasMany('App\Student');
    }

    public function section()
    {
        return $this->hasMany('App\Section');
    }
}
