<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'subject_code','subject','semester_id'
    ]; 

    public function semester()
    {
        return $this->belongsTo('App\Semester');
    }

}
