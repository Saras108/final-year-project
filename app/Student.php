<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'slc_marks', 'intermediate_marks', 'slc_batch','intermediate_batch', 'slc_symbol', 'intermediate_symbol','p_address','t_address','section_id',
    ];

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    public function guardgain()
    {
        return $this->hasmany('App\Guard');
    }

    public function result()
    {
        return $this->hasmany('App\Result');
    }


}