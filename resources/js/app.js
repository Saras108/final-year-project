/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

import { Form, HasError, AlertError } from 'vform'

window.Form = Form;

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)


import VueRouter from 'vue-router'
Vue.use(VueRouter);

import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.Toast =  Toast;

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
})
 
let routes = [
    { 
        path: '/dashboard', 
        component: require('./dashboard/dashboard.vue').default
    },

    {
      path: '/allbatch',
      component: require('./batch/batchlist.vue').default 
    },

    {
      path: '/allstaff',
      component: require('./staff/stafflist.vue').default 
    },

    {
      path: '/authuser',
      component: require('./authuser.vue').default 
    },

    {
      path: '/allstudents',
      component: require('./students/studentlist.vue').default 
    },
    
    {
      path: '/semester',
      component: require('./semester/semesterlist.vue').default 
    },
    
    {
      path: '/allexam',
      component: require('./exam/examlist.vue').default 
    },
    
    {
      path: '/examtobe/:id',
      name:'examtobe',
      props: true,
      component: require('./exam/examtobe.vue').default
    },

    {
      path: '/ourcourse',
      component: require('./course/courselist.vue').default 
    },
    
    {
      path: '/batchstudents/:year/:id',
      name:'batchstudents',
      props: true,
      component: require('./batch/batchstudents.vue').default 
    },
    
    {
      path: '/rotuine/:session/:exam',
      name:'rotuine',
      props: true,
      component: require('./exam/examrotuine.vue').default 
    },
    
    {
      path: '/marks/:session/:exam',
      name:'marks',
      props: true,
      component: require('./marks/coursemarks.vue').default 
    },
    
    {
      path: '/batchsection/:batch',
      name:'batchsection',
      props: true,
      component: require('./section/batchsection.vue').default 
    },
    
    {
      path: '/sectionstudent/:section',
      name:'sectionstudent',
      props: true,
      component: require('./section/sectionstudent.vue').default 
    },
    
    {
      path: '/studentinfo/:id',
      name:'studentinfo',
      props: true,
      component: require('./students/studentinfo.vue').default
    },

    {
      path: '/semestercourse/:id',
      name:'semestercourse',
      props: true,
      component: require('./semester/semestercourse.vue').default
    },
    
    {
      path: '/studentmarks/:id',
      name:'studentmarks',
      props: true,
      component: require('./marks/studentmarks.vue').default
    }
  ]

window.Fire =new Vue();


 
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

const app = new Vue({
    el: '#app',
    router
});