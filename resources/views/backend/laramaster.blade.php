
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>AdminLTE 3 | Starter</title>

        <link rel="stylesheet" href="/css/app.css">

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

 @include('backend.layouts.topbar')
 @include('backend.layouts.sidebar')

     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
<!--         
         <router-view></router-view>
         <vue-progress-bar></vue-progress-bar> -->

        @yield('content')

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('backend.layouts.footer')


</div>
<!-- ./wrapper -->


<!-- AdminLTE App -->
<script src="/js/app.js"></script>
</body>
</html>
