@extends('backend.laramaster')
@section('content')
            <div class="row justify-content-center mt-4">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Section List for <b>{{$info->batch->enrolled_year}}</b> batch <b>{{$info->semester->semester}}</b> for <u>Subject:: {{$course->subject}}</u> <b>{{$section->section_name}} Section</b></h3>
                
                <div class="card-tools">
        

                </div>

              </div>
              <!-- /.card-header -->

              <div class="card-body table-responsive pt-10">

                <div class="row">
                <form action="{{route('marks.store')}}" method="post">
                @csrf()

                @foreach($students as $student)
                    <div class="col-lg-12 col-12">
                   
                        <!-- small box -->
                        <div class="small-box">
                            <div class="inner">
                                <h4>Roll:{{$student->roll_no}}</h4>
                                <p>{{$student->person->fname}}</p>
                                 <input type="hidden" name="student[]" value={{$student->id}}>
                                 <input type="hidden" name="exam" value={{$exam->id}}>
                                 <input type="hidden" name="course" value={{$course->id}}>
                                 <input type="hidden" name="session" value={{$session}}>
                       
                                    @foreach($marks as $mark)
                                        @if($mark[0]->student_id == $student->id)
                                        <input type="text" name="markssetter[]" id="markssetter"  value= "{{$mark[0]->marks}}" class="form-control" placeholder="Enter Marks.">
                                        <input type="hidden" name="myid[]" value={{$mark[0]->id}}>                                
                                        @endif
                                        
                                    @endforeach
                            </div>               
                        </div>
                       
                    </div>
                    @endforeach

                    <button type="submit">Submit/Update</button>
                   
                </form>
                </div>
            </div>

            </div>
            </div>

        </div>
        
@endsection