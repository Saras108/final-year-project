  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PMC, PatanDhoka</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{ Auth::user()->name }} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <router-link to="/allbatch" class="nav-link">
              <i class="nav-icon fas fa-heading"></i>
              <p>
                Batch
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <router-link to="/allstudents" class="nav-link">
              <i class="fas fa-users"></i>
              <p> All Students</p>
            </router-link>
          </li>
          
          <li class="nav-item">
            <router-link to="/allstaff" class="nav-link">
              <i class="fas fa-paper-plane"></i>
              <p>All Staff</p>
            </router-link>
          </li>
          
          <li class="nav-item">
            <router-link to="/authuser" class="nav-link">
              <i class="fas fa-user"></i>
              <p>Auth User</p>
            </router-link>
          </li>
          
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-university"></i>
              <p>Acadamic               
              <i class="right fas fa-angle-left"></i>
               </p>
            </a>
            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/semester" class="nav-link">
                  <i class="fas fa-tasks ml-3"></i>
                  <p>Semester</p>
                </router-link>
              </li>
              <li class="nav-item">
                <a href="/allexam" class="nav-link">
                  <i class="fas fa-road ml-3"></i>
                  <p>Exam</p>
                </a>
              </li>
              <li class="nav-item">
                <router-link to="/ourcourse" class="nav-link">
                  <i class="fas fa-book ml-3"></i>
                  <p>Courses</p>
                </router-link>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
              <i class="nav-icon fas fa-power-off red"></i>
              <p>
              {{ __('Logout') }}
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
