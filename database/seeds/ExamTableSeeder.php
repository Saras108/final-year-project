<?php

use Illuminate\Database\Seeder;

class ExamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exams')->insert([
            'exam_name' => 'First Assesment',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('exams')->insert([
            'exam_name' => 'Second Assesment',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('exams')->insert([
            'exam_name' => 'Board',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('exams')->insert([
            'exam_name' => 'Practical',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
