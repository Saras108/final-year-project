<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $user = new User();
        // $user->name = "Saras";
        // $user->email = 'saras.me108@gmail.com';
        // $user->password = bcrypt('123123123');
        // $user->email_verified_at = now();
        // $user->added_by = 0;
        // $user->save();

        User::truncate();
        $admin = User::create([
            'name'=> 'Saras',
            'email'=> 'saras.me108@gmail.com',
            'password'=> bcrypt('123123123'),
            'email_verified_at'=>now(),
            'added_by'=>0,
        ]);
    }
}
